import {Injectable} from "@angular/core";
import {UmbilicalServiceCompany} from "@ngx-kz/mybpm-models";
import {InOutService} from "../plugin-in-out/in-out.service";
import {Observable} from "rxjs";

@Injectable({providedIn: 'root'})
export class PluginCompanyService extends UmbilicalServiceCompany {
  constructor(
    private readonly inOutService: InOutService,
  ) {
    super();
  }

  refreshCompanyPageData$(): Observable<void> {
    return this.inOutService.refreshCompanyPageData$;
  }
}
