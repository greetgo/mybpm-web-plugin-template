import {Injectable} from "@angular/core";
import {UmbilicalServiceBo} from "@ngx-kz/mybpm-models";
import {InOutService} from "../plugin-in-out/in-out.service";
import {Observable} from "rxjs";

@Injectable({providedIn: 'root'})
export class PluginBoService extends UmbilicalServiceBo {
  constructor(
    private readonly inOutService: InOutService,
  ) {
    super();
  }

  refreshBoiList$(): Observable<void> {
    return this.inOutService.refreshBoiList$;
  }
}
