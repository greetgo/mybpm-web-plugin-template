import {UmbilicalServiceNotification} from "@ngx-kz/mybpm-models";
import {Injectable} from "@angular/core";
import {InOutService} from "../plugin-in-out/in-out.service";
import {Observable} from "rxjs";

@Injectable({providedIn: 'root'})
export class PluginNotificationService extends UmbilicalServiceNotification {

  constructor(
    private readonly inOutService: InOutService,
  ) {
    super();
  }

  showInfoNotification$(): Observable<string> {
    return this.inOutService.showInfoNotification$;
  }
}
