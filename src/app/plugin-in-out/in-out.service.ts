import {Injectable} from "@angular/core";
import {Observable, Subject} from "rxjs";

@Injectable({providedIn: 'root'})
export class InOutService {
  private showInfoNotificationSubject: Subject<string> = new Subject<string>();
  readonly showInfoNotification$: Observable<string> = this.showInfoNotificationSubject.asObservable();

  private refreshCompanyPageDataSubject: Subject<void> = new Subject<void>();
  readonly refreshCompanyPageData$: Observable<void> = this.refreshCompanyPageDataSubject.asObservable();

  private refreshBoiListSubject: Subject<void> = new Subject<void>();
  readonly refreshBoiList$: Observable<void> = this.refreshBoiListSubject.asObservable();

  showInfoNotification(message: string): void {
    this.showInfoNotificationSubject.next(message);
  }

  refreshCompanyPageData(): void {
    this.refreshCompanyPageDataSubject.next();
  }

  refreshBoiList(): void {
    this.refreshBoiListSubject.next();
  }
}
