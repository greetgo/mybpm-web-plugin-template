import {Component} from "@angular/core";
import {
  UmbilicalServiceBo,
  UmbilicalServiceCompany,
  UmbilicalServiceCord,
  UmbilicalServiceNotification
} from "@ngx-kz/mybpm-models";
import {PluginNotificationService} from "../plugin-service/plugin-notification.service";
import {PluginBoService} from "../plugin-service/plugin-bo.service";
import {PluginCompanyService} from "../plugin-service/plugin-company.service";

@Component({
  selector: 'app-in-out',
  template: '',
})
export class InOutComponent extends UmbilicalServiceCord {

  constructor(
    private readonly pluginBoService: PluginBoService,
    private readonly pluginCompanyService: PluginCompanyService,
    private readonly pluginNotificationService: PluginNotificationService,
  ) {
    super();
  }

  get bo(): UmbilicalServiceBo {
    return this.pluginBoService;
  }

  get company(): UmbilicalServiceCompany {
    return this.pluginCompanyService;
  }

  get notification(): UmbilicalServiceNotification {
    return this.pluginNotificationService;
  }
}
