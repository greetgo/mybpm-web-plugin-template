import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {MenuItemComponent} from "./menu-item.component";
import {MenuItemModule} from "@mybpm.workspace/menu-item";
import {InOutComponent} from "./plugin-in-out/in-out.component";

@NgModule({
  declarations: [
    AppComponent,
    MenuItemComponent,
    InOutComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MenuItemModule,
  ],
  exports: [AppComponent, MenuItemComponent, InOutComponent],
  bootstrap: [AppComponent]
})
export class AppModule {
}
