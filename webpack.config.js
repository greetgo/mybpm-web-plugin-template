const {withModuleFederationPlugin} = require('@angular-architects/module-federation/webpack');

const path = require("path");
const mf = require("@angular-architects/module-federation/webpack");
const share = mf.share;

const sharedMappings = new mf.SharedMappings();
sharedMappings.register(path.join(__dirname, './tsconfig.json'), [/* mapped paths to share */]);

module.exports = withModuleFederationPlugin({
  name: 'plugins',
  library: {type: 'var', name: 'plugins'},
  filename: 'plugin.js',
  exposes: {
    './MenuItemPlugin': './src/app/menu-item.component.ts',
    './ExamplePagePlugin': './src/app/example-page/example-page.module',
    './InOutPlugin': './src/app/plugin-in-out/in-out.component.ts',
  },
  shared: share({
    '@angular/animations': {singleton: true, requiredVersion: '^15.0.2'},
    '@angular/core': {singleton: true, requiredVersion: '^15.0.2'},
    '@angular/common': {singleton: true, requiredVersion: '^15.0.2'},
    '@angular/forms': {singleton: true, requiredVersion: '^15.0.2'},
    '@angular/router': {singleton: true, requiredVersion: '^15.0.2'},
    '@angular/cdk': {singleton: true, requiredVersion: '^15.0.1'},
    '@angular/material/icon': {singleton: true, requiredVersion: '^15.0.1'},
    'ng-zorro-antd': {singleton: true, requiredVersion: '^14.2.1'},
    'ngx-mask': {singleton: true, requiredVersion: '^14.0.3'},
    'ngx-infinite-scroll': {singleton: true, requiredVersion: '^14.0.0'},
    '@ngx-translate/core': {singleton: true, requiredVersion: '^14.0.0'},
    '@ngx-translate/http-loader': {singleton: true, requiredVersion: '^7.0.0'},
    '@angular/flex-layout': {singleton: true, requiredVersion: '~14.0.0-beta.40'},
    'rxjs': {singleton: true, requiredVersion: '~7.5.6'},

    ...sharedMappings.getDescriptors(),
  })
});

module.exports.output = {
  uniqueName: "plugins",
  publicPath: "auto",
  scriptType: 'text/javascript'
};

module.exports.experiments = {outputModule: true};
module.exports.optimization = {runtimeChunk: false};
