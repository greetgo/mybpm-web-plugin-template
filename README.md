[comment]: <> (###PIN m1 MODIFY replace template {PLUGIN_NAME.dashed})

# mybpm-web-plugin-template

Доступные типы плагинов, на данный момент:

<p>CONSOLE - Текст в консоль</p>
<p>MENU_ITEM - Добавление меню в список</p>
<p>PAGE - Отдельная страница</p>
<p>OVERLAY - Оверлей на главный экран</p>
<p>IN_OUT - Взаимодействие между ядром и плагином</p>
<p>COMPANY_PAGE_HEADER_CONTENT - Контент в шапку страницы компании</p>
<p>BO_VIEWER_HEADER_CONTENT - Контент в шапку реестра бо</p>
<p>REGISTER_PAGE_BOTTOM_CONTENT - Контент под кнопку в регистрации</p>
<p>AUTH_ADDITIONAL_LOGO - Внешний логотип на странице авторизации</p>
<p>HEADER_ADDITIONAL_LOGO - Внешний логотип в шапке системы</p>
